#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <windows.h>
#include <json-c/json.h>

int primogems = 79000;
int gacha = 0, status = 0;




// 1a.
/* Get ZIP folder */
void downLoad(){


  /* Download weapons file */
  if (fork() == 0) {
    
    char * weaponsZip[] = {"wget", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "Weapons.zip", NULL};
    execv("./bin/wget", weaponsZip);
  }



  /* Download char file */
  if (fork() == 0){
    
    char * charZip[] = {"wget", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "Characters.zip", NULL};
    execv("./bin/wget", charZip);
  }

    while(wait(NULL)>=0);
}


// 1b.
void unZip() {

  /* Unzip char file */
  if (fork() == 0){
    
    char * unZipChar[] = {"unzip", "Characters.zip", NULL};
    execv("./bin/unzip", unZipChar);
  }

  /* Unzip weapons file */
  if (fork() == 0){
    
    char * unZipWeap[] = {"unzip", "Weapons.zip", NULL};
    execv("./bin/unzip", unZipWeap);
  }

    while(wait(NULL)>=0);


  /* Remove char file */
  if (fork() == 0){
    
    char * rmChar[] = {"rm", "Characters.zip", NULL};
    execv("./bin/mkdir", rmChar);
  }

  /* Remove weapons file */
  if (fork() == 0){
    
    char * rmWeapons[] = {"rm", "Weapons.zip", NULL};
    execv("./bin/mkdir", rmWeapons);
  }

  /* mkdir gacha_gacha */
  if (fork() == 0){

    char * mkDirectory[] = {"mkdir", "gacha_gacha", NULL};
    execv("./bin/mkdir", mkDirectory);
  }
  
    while(wait(&status) >= 0);

    chdir("./gacha_gacha");  
  


  else {

    printf("Failed to load");

    return -1;

  }

}


// 1c.
char nameFiles() {

  char path[1000], result, * base;
  int nGacha, i, found = 0;

  struct mkdirectory *dt;

  DIR * dir = opendir(base);

  while ((dt = readdir(dir)) != NULL && found == 0) {

        if (strcmp(dt -> d_name, ".") != 0 && strcmp(dt -> d_name, "..") != 0) {

            result = dt -> d_name;

            if (i == nGacha) { found = 1; }

            i++;
        }
  }
  closedir(dir);
  return result;

}


// Main driver function
int main() {

  downLoad();
  unZip(status);
  nameFiles();

  return 0;
}
