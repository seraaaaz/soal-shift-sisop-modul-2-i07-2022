#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <errno.h>
#include <limits.h>
#include <unistd.h>
#include <pwd.h>
#include <dirent.h>
#include <syslog.h>
#include <string.h>

void forked (char exe[], char *var[]) 
{ 
    int status; 
    pid_t child_id;
    child_id = fork();
    if(child_id == 0){
        execv(exe, var);
    } else
    {
        // ((wait(&time))>3);
        return; 
    } 
}


void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "cp", "{}", destination, ";", NULL};
       forked("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void deletefolder (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        forked("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void delete (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        forked("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}

void next(){
    pid_t child_id = fork();
    if (child_id < 0)
    {
        exit(EXIT_FAILURE);
    }
    if(child_id == 0){
        // Unzip();
        char destawal[101] = "/Users/selomitazhafiirah/soal3";
        char destiunzip[101] = "/Users/selomitazhafiirah/soal3/animal";
        char Destair[101] = "/Users/selomitazhafiirah/soal3/air";
        char Destdar[101] = "/Users/selomitazhafiirah/soal3/darat";
        move(destiunzip, Destair, "*air*");
        move(destiunzip, Destdar, "*darat*");
        
        
        delete(Destdar, "*bird*");
        remove(destiunzip);

        struct dirent *folder;
        DIR *dir;
        while (folder == readdir(dir))
        {
            char pathFileLama[100] = "/Users/selomitazhafiirah/soal3";
            strcat(pathFileLama, "/soal3/animal/");
            strcat(pathFileLama, folder->d_name);
            remove(pathFileLama);
        }
        
    }
    else{
        wait(NULL);
    }
}


void create_list(char *place){
    FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/Users/selomitazhafiirah/soal3/");
    strcat(path, place);
    strcat(path, "/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);


    p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

   
    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    while((dp = readdir(dir))!= NULL){
        if(strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 ){
            
           
            
            char owner[NAME_MAX];
            char p_r[NAME_MAX];
            char p_w[NAME_MAX];
            char p_x[NAME_MAX];
            
            
            struct passwd *pw = getpwuid(info.st_uid);
            if (pw != 0) strcpy(owner, pw->pw_name); 

         
            if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
            if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
            if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

            if(strstr(dp->d_name, "list") == 0){
                fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
            }
        
        }
    }

    fclose(fp);
    return;
}

void data_list(){
    pid_t child = fork();
    int status;

    if (child < 0) exit(EXIT_FAILURE);

    if (child == 0){
        // create_list("air");
    }else{
        while((wait(&status)) > 0);
        create_list("air");
    }
}



int main(){
int time;
char folderDarat[100] = {"/Users/selomitazhafiirah/soal3/darat"};
char folderAir[100] = {"/Users/selomitazhafiirah/soal3/air"};
char extractzip[101] = "/Users/selomitazhafiirah/soal3";
char zipPlace[101] = "/Users/selomitazhafiirah/Downloads/animal.zip";

pid_t child_id;
child_id = fork();
if(child_id < 0){
    exit(EXIT_FAILURE);
}
if(child_id == 0){
    char *createFolderDarat[]={"mkdir", "-p", folderDarat, NULL};
    forked("/bin/mkdir", createFolderDarat);
    char *createFolderAir[]={"mkdir", "-p", folderAir, NULL};
    forked("/bin/mkdir", createFolderAir);

    char *unzip[] = {"unzip", "-q", zipPlace, "-d", extractzip, "*.jpg", NULL};
    forked("/usr/bin/unzip", unzip);
}
else {
    while ((wait(&time))>0);
}
 next();
    data_list();


}
