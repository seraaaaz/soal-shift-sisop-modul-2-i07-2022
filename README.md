# soal-shift-sisop-modul-2-I07-2022
## **Laporan Resmi dan Penjelasan Soal Shift Modul 2**
### **Kelompok I-07**
**Anggota :**
- Selomita Zhafirah 5025201120
- Amelia Mumtazah Karimah 5025201128
- Mohammed Fachry Dwi Handoko 5025201159

## **Daftar Isi Lapres**
- [Soal-1](#soal1)
- [Soal-2](#soal2)
- [Soal-3](#soal3)

# Soal 1


Include both standard and external libraries using a jSON extraction library.

Declare primogems with a value of 79000.
Declare gacha and status as 0.


[
   #include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <syslog.h>
#include <errno.h>
#include <dirent.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <time.h>
#include <wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <windows.h>
#include <json-c/json.h>

int primogems = 79000;
int gacha = 0, status = 0;

],


1a.  Download zip files for both weapons and characters from an external GDrive link using a utility function 

[
	/* Get ZIP folder */
void downLoad(){


  /* Download weapons file */
  if (fork() == 0) {
    
    char * weaponsZip[] = {"wget", "https://docs.google.com/uc?export=download&id=1XSkAqqjkNmzZ0AdIZQt_eWGOZ0eJyNlT", "-O", "Weapons.zip", NULL};
    execv("./bin/wget", weaponsZip);
  }



  /* Download char file */
  if (fork() == 0){
    
    char * charZip[] = {"wget", "https://docs.google.com/uc?export=download&id=1xYYmsslb-9s8-4BDvosym7R4EmPi6BHp", "-O", "Characters.zip", NULL};
    execv("./bin/wget", charZip);
  }

    while(wait(NULL)>=0);
}


],

1b.  Unzip files into folders using an array to store arguments to their respective directories (user/bin/.)


[
	void unZip() {

  /* Unzip char file */
  if (fork() == 0){
    
    char * unZipChar[] = {"unzip", "Characters.zip", NULL};
    execv("./bin/unzip", unZipChar);
  }

  /* Unzip weapons file */
  if (fork() == 0){
    
    char * unZipWeap[] = {"unzip", "Weapons.zip", NULL};
    execv("./bin/unzip", unZipWeap);
  }

    while(wait(NULL)>=0);


  /* Remove char file */
  if (fork() == 0){
    
    char * rmChar[] = {"rm", "Characters.zip", NULL};
    execv("./bin/mkdir", rmChar);
  }

  /* Remove weapons file */
  if (fork() == 0){
    
    char * rmWeapons[] = {"rm", "Weapons.zip", NULL};
    execv("./bin/mkdir", rmWeapons);
  }

  /* mkdir gacha_gacha */
  if (fork() == 0){

    char * mkDirectory[] = {"mkdir", "gacha_gacha", NULL};
    execv("./bin/mkdir", mkDirectory);
  }
  
    while(wait(&status) >= 0);

    chdir("./gacha_gacha");  
  


  else {

    printf("Failed to load");

    return -1;

  }

}

],


Remove zip files and make a directory without mkdir( ).
Create a folder with the name gacha_gacha.

1c.
Rename files according to directory location, search and flag for naming while open the directory once done, read the number of Gacha files, then close the directory. 


[
	// 1c.
char nameFiles() {

  char path[1000], result, * base;
  int nGacha, i, found = 0;

  struct mkdirectory *dt;

  DIR * dir = opendir(base);

  while ((dt = readdir(dir)) != NULL && found == 0) {

        if (strcmp(dt -> d_name, ".") != 0 && strcmp(dt -> d_name, "..") != 0) {

            result = dt -> d_name;

            if (i == nGacha) { found = 1; }

            i++;
        }
  }
  closedir(dir);
  return result;

}


// Main driver function
int main() {

  downLoad();
  unZip(status);
  nameFiles();

  return 0;
}

]


Functions are recalled in the main driver functions.



# Soal 2
## **2a**
Inti dari soal 2a adalah meminta Japrun untuk mengunzip file .png yang ada didalam ```drakor.zip```  lalu membuat folder baru di ```/home/[user]/shift2/drakor```. Karena atasan Japrun teledor, dalam zip tersebut bisa berisi folder-folder yang tidak penting, maka program harus bisa membedakan file dan folder sehingga dapat memproses file yang seharusnya dikerjakan dan menghapus folder-folder yang tidak dibutuhkan.

Langkah pertama, kami membuat beberapa deklarasi library untuk menjalankan program ini.
```bash
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <grp.h>
#include <wait.h>
#include <pwd.h>
#include <string.h>
#include <dirent.h>
```
Lalu, membuat beberapa ```variabel``` untuk menggantikan gambar proses saat ini dengan gambar proses baru.
```bash
int main(){
int time;
char folderDrakor[] = {"/Users/ameliamumtazah/Documents/shift2/drakor"};
char filename[] = {"/Users/ameliamumtazah/Documents/shift2/drakor"};
char extractzip[] = "/Users/ameliamumtazah/Documents/shift2/drakor";
char deletezip [] = "/Users/ameliamumtazah/Documents/shift2/drakor"
char zipPlace[] = "/Users/ameliamumtazah/Documents/sisop/drakor.zip";
```
Kemudian, pada fungsi main akan memanggil beberapa fungsi untuk melakukan program yang diminta. Pertama membuat folder drakor yang ada dalam folder shift2 yang ada dalam home directory.
```bash
void makedir(char *dest) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", dest, NULL};
		execv("/Users/ameliamumtazah/Documents/shift2/drakor", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```
Selanjutnya, kita melakukan extract zip drakor di ```/home/[user]/shift2/drakor```. 
```bash
void Extractzip(){
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"unzip", "-j", "drakor.zip", "*.png", "-d", "/home/ameliamumtazah/shift2/drakor", NULL};
		execv("/Users/ameliamumtazah/Documents/shift2/drakor", argv);
	} else {
		((wait(&status)) > 0);
	}
```
Kemudian, kita akan menghapus folder hasil extract zip yang tidak berguna.
```bash
void delete (char *file) {
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"rm", "-d", file, NULL};"/Users/ameliamumtazah/Documents/shift2/drakor", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```
![2a](/uploads/2800f53e646eb42f3ef8b42ab507fc83/2a.png)
## **2b**
Inti dari soal 2b adalah membuat sebuah fungsi ```createFolderDrakorSortbyCategory()``` untuk mengkategorikan poster yang sesuai dengan kategorinya. Selanjutnya, kita akan menjalankan fungsi Execuv() untuk dapat mengeksekusi fungsi makedir(). Lalu, membuat fungsi strtok()yang akan berfungsi untuk memisahkan setiap potongan nama file.
```bash
void listFilesRecursively(char *basePath) {
	char path[1000];
	struct dirent *dp;
	DIR *dir = opendir(basePath);

	if (!dir)
		return;

	while ((dp = readdir(dir)) != NULL) {
		if (strcmp(dp->d_name, ".") != 0 && strcmp(dp->d_name, "..") != 0 && strcmp(dp->d_name, "my-roomate-is-a-gumiho;2021;fantasy;.png") != 0);
		char tempFolder[100];
		char mkFolder[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
		char Filename[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
        char action[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
		char fantasy[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
		char thriller[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
        char horror[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
        char romance[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
        char school[100] = "/Users/ameliamumtazah/Documents/shift2/drakor";
		char *token, *token2, *token3;

        //Untuk membuat dir dengan kategori posternya
		strcpy(tempFolder, dp->d_name);
		strtok(tempFolder, ";");
		strcat(mkFolder, tempFolder);
```
Kode tersebut merupakan potongan kode untuk menjawab sub soal 2b, yang pertama terdapat array tempFolder yang berisi hasil copy dari dp->d_name. dp->d_name ini berisi list file sebagai contohnya my-roomate-is-a-gumiho;2021;fantasy;.png. Kemudian menggunakan strtok dengan delimiter ; agar bisa mendapatkan kategori poster setiap file seperti romance. Setelah mendapatkan semua kategori poster tersebut kemudian digabung semua list kategori poster tersebut ke dalam array baru bernama mkFolder yang nanti isi dari array mkFolder ialah seperti /Users/ameliamumtazah/Documents/shift2/drakor/{kategori drama}. Setelah itu menggunakan fungsi makedir() yang sebelumnya sudah dijelaskan diatas untuk membuat folder tiap kategori dramanya.
![Screen_Shot_2022-03-27_at_16.28.08](/uploads/66ab3d491085d155560a6a9c66ab8658/Screen_Shot_2022-03-27_at_16.28.08.png)
## **2c**
Permasalahan dari sub soal 2c ialah memindahkan foto ke folder dengan kategori yang sesuai dan di rename dengan nama. Contoh: “/drakor/romance/start-up.png”.
```bash
			if (!(strstr(dp->d_name, "_"))) {
				strcpy(temp, dp->d_name); 
				token = strtok(temp, ";"); 
				strcpy(getJenis, token);
				token = strtok(NULL, ";"); 
				strcpy(getNama, token);
				token = strtok(NULL, ";"); 
				strcat(posterDua, getJenis);
				strcat(posterDua, "/");
				strcat(posterDua, getNama);
				strcat(posterDua, ".png");
			}
```
![2c](/uploads/fc85fa59b7e50790994a8b9486a7ff91/2c.png)
## **2d**
Permasalahan dari sub soal 2d ialah karena dalam satu foto bisa terdapat lebih dari satu poster  maka foto harus di pindah ke masing-masing kategori yang sesuai.
```bash
if (strstr(dp->d_name, "_")) {
		strcpy(temp3, dp->d_name); 
		token3 = strtok(temp3, ";"); 
		strcpy(getJenis2, token3);
		token3 = strtok(NULL, ";"); 
		trcpy(getNama2, token3);
		token3 = strtok(NULL, ";");
				
        strcpy(getTahun, token3);
		strcat(posterDua, getJenis2);
		strcat(posterDua, "/");
		strcat(posterDua, getNama2);
		strcat(posterDua, ".png");
				
		strcpy(temp2, dp->d_name); 
		token2 = strtok(temp2, "_"); 
		token2 = strtok(NULL, "_");	 
		strcpy(temp4, token2);
		token2 = strtok(temp4, ";"); 
		strcpy(getJenis3, token2);
		token2 = strtok(NULL, ";"); 
		strcpy(getNama3, token2);
		token2 = strtok(NULL, ";"); 
		strcpy(getTahun, token2);
		strcat(posterTiga, getJenis3);
		strcat(posterTiga, "/");
		strcat(posterTiga, getNama3);
		strcat(posterTiga, ".png");
}
```
## **2e**
Inti dari sub soal 2e ialah membuat file data.txt di masing-masing folder kategori drama korea, dan file tersebut berisi nama dan tahun rilis semua drama korea dalam folder tersebut, berurutan berdasarkan tahun rilis. Seperti conoth dibawah ini :
```bash
kategori : romance

nama : start-up 
rilis : tahun 2020

nama : twenty-one-twenty-five
rilis : tahun 2022
```
Dibawah ini merupakan potongan kode untuk soal 2e :
```bash
FILE *fptr;
			char fname[50];
			strcpy(fname, mkFolder);
			strcat(fname, "/data.txt");
			fptr = fopen(fname, "a+");
			if (!(strstr(dp->d_name, "_"))) {
				fprintf(fptr, "nama : %s\n", getNama);
				fprintf(fptr, "tahun : %s tahun\n\n", cut_four(getTahun));
			}
```
Menggunakan fungsi ```fopen``` dengan  ```a+``` agar bisa mengisi data yang ada di ```/Users/ameliamumtazah/Documents/shift2/drakor/{kategori drama}/data.txt```. Kemudian didalam file ```data.txt``` tersebut terdalam data nama dan tahun rilis, untuk tahun ini menggunakan fungsi baru bernama cut_four karena output dari getTahun. Kemudian untuk menghilangkan .png menggunakan fungsi sebagai berikut :
```bash
char* cut_four (char* s) {
    int n;
    int i;
    char* new;
    for (i = 0; s[i] != '\0'; i++);
    // lenght of the new string
    n = i - 4 + 1;
    if (n < 1)
        return NULL;
    new = (char*) malloc (n * sizeof(char));
    for (i = 0; i < n - 1; i++)
        new[i] = s[i];
    new[i] = '\0';
    return new;
}
```
![2d](/uploads/2e7dd1aed094fc2f1e86374d8090f92b/2d.png)
![Screen_Shot_2022-03-27_at_16.54.09](/uploads/95ca63e067ea7fac8edabf97b4ec7e9d/Screen_Shot_2022-03-27_at_16.54.09.png)

# Soal 3
## **3a**
Membuat 2 directory "/Users/selomitazhafiirah/soal3/" dengan nama darat dan air yang berselang 3 detik melalui char *folderDarat dan char *folderAir artinya menyimpan semua fungsi seperti mkdir. Disini menggunakan execv atau argv untuk membuat directory lalu memanggil fungsi forked dan meletakkan mkdir dan memasukkan folder create di char. darat dan air di dalam {} artinya dimana letak lokasi yang ingin kita buat.
```c 
char folderDarat[100] = {"/Users/selomitazhafiirah/soal3/darat"};
char folderAir[100] = {"/Users/selomitazhafiirah/soal3/air"};
char extractzip[101] = "/Users/selomitazhafiirah/soal3";
char zipPlace[101] = "/Users/selomitazhafiirah/Downloads/animal.zip";

pid_t child_id;
child_id = fork();
if(child_id < 0){
    exit(EXIT_FAILURE);
}
if(child_id == 0){
    char *createFolderDarat[]={"mkdir", "-p", folderDarat, NULL};
    forked("/bin/mkdir", createFolderDarat);
    char *createFolderAir[]={"mkdir", "-p", folderAir, NULL};
    forked("/bin/mkdir", createFolderAir);
```
## **3b**
Kemudian program diminta dapat melakukan extract “animal.zip” di “/home/[USER]/modul2/”.
buat lokasi dimana animal.zip berada.
```c
char extractzip[101] = "/Users/selomitazhafiirah/soal3";
char zipPlace[101] = "/Users/selomitazhafiirah/Downloads/animal.zip";
```
Buat menggunakan execv "unzip" untuk meng-unzip semua file yang berisi jpg (*.jpg) zip tersebut menggunakan fungsi forked.
```c
char *unzip[] = {"unzip", "-q", zipPlace, "-d", extractzip, "*.jpg", NULL};
    forked("/usr/bin/unzip", unzip);
```
## **3c**
hasil extract dipisah menjadi hewan darat dan hewan air sesuai dengan nama filenya. Untuk hewan darat dimasukkan ke folder “/home/[USER]/modul2/darat” dan untuk hewan air dimasukkan ke folder “/home/[USER]/modul2/air”. Rentang pembuatan antara folder darat dengan folder air adalah 3 detik dimana folder darat dibuat terlebih dahulu. Untuk hewan yang tidak ada keterangan air atau darat harus dihapus.

membuat fungsi yang disebut move. Di dalam function move kita memiliki char *source untuk lokasi folder, destination untuk tujuan output, searchname untuk nama pencarian yang ingin kita pindahkan.
```c
void move (char *source, char *destination,char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "cp", "{}", destination, ";", NULL};
       forked("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}
```
setelah itu cari dari sumber dengan nama yang ada air/darat dan pindahkan ke tempat tujuan.
```c
char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "mv", "{}", destination, ";", NULL};
```
run fungsi
```c
move(destiunzip, Destair, "*air*");
move(destiunzip, Destdar, "*darat*");
```
## **3d**
Setelah berhasil memisahkan hewan berdasarkan hewan darat atau hewan air. Dikarenakan jumlah burung yang ada di kebun binatang terlalu banyak, maka pihak kebun binatang harus merelakannya sehingga conan harus menghapus semua burung yang ada di directory “/home/[USER]/modul2/darat”. Hewan burung ditandai dengan adanya “bird” pada nama file.
Sama seperti sebelumnya (3c), ubah dari mv ke rm yang rm. pertama kita cari dari source dengan nama searchname, dan tidak ada tujuan karena kita menghapus file bukan memindahkan file.
```c
void delete (char *source, char *searchname){
    pid_t child_id = fork();
    int status;
    if(child_id == 0){
        char *moveGenre[] = {"find", source, "-iname", searchname, "-exec", "rm", "{}", ";", NULL};
        norepeatfork("/usr/bin/find", moveGenre);
    }
    else{
        wait(NULL);
    }
}
```
run fungsi
```c
delete(Destdar, "*bird*");
```
## **3e**
Terakhir, Conan harus membuat file list.txt di folder “/home/[USER]/modul2/air” dan membuat list nama semua hewan yang ada di directory “/home/[USER]/modul2/air” ke “list.txt” dengan format UID_[UID file permission]_Nama File.[jpg/png] dimana UID adalah user dari file tersebut file permission adalah permission dari file tersebut. Contoh : conan_rwx_hewan.png
Membuat variabel yang berisi tujuan directory dan juga membuat list.txt dan membuka menggunakan fopen di dalam fp.
```c
FILE *fp;
    DIR *dir;
    struct stat info;
    struct stat fs;
    struct dirent *dp;
    int r;
    int p;

    char path[PATH_MAX];
    char permission[NAME_MAX];
    char list[NAME_MAX];

    strcpy(path, "/Users/selomitazhafiirah/soal3/");
    strcat(path, place);
    strcat(path, "/");
    strcpy(list, path);
    strcat(list, "list.txt");
    fp = fopen(list, "a");
    dir = opendir(path);
```
menggunakan izin users sehingga kita perlu mendapatkan izin menggunakan pwuid. 
```c
p = stat(path, &fs);
    if (p == -1){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    r = stat(path, &info);
    if( r==-1 ){
        fprintf(stderr,"File error\n");
        exit(1);
    }

    struct passwd *pw = getpwuid(info.st_uid);
    if (pw != 0) strcpy(owner, pw->pw_name);
```
kemudian, salin r, w, dan x dan mencetak sebagai nama file di list.txt
```c
if (fs.st_mode & S_IRUSR) strcpy(p_r, "r");
if (fs.st_mode & S_IWUSR) strcpy(p_w, "w");
if (fs.st_mode & S_IXUSR) strcpy(p_x, "x");

if(strstr(dp->d_name, "list") == 0){
    fprintf(fp, "%s_%s%s%s_%s\n", owner, p_r, p_w, p_x, dp->d_name);
}
```

### OUTPUT
![](https://i.ibb.co/HGTwxK8/Screen-Shot-2022-03-27-at-21-31-46.png)

![](https://i.ibb.co/d0ZtMvG/Screen-Shot-2022-03-27-at-21-31-56.png)

![](https://i.ibb.co/FKGCdyh/Screen-Shot-2022-03-27-at-21-32-07.png)

![](https://i.ibb.co/9cXVNpv/Screen-Shot-2022-03-27-at-21-32-18.png)

![](https://i.ibb.co/bX8nQh9/Screen-Shot-2022-03-27-at-21-32-35.png)



